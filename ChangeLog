   _        ___  ____ ____  ____
  |_|_ _   / _ \/ ___/ ___||  _ \   _____   _____
  _|_||_| | | | \___ \___ \| |_) | / __\ \ / / __|
 |_||_|_| | |_| |___) |__) |  __/  \__ \\ V /\__ \
  |_|_|_|  \___/|____/____/|_|     |___/ \_/ |___/

  OSSP svs - Stupid/Silly/Simple Versioning System

  ChangeLog

  Changes between 1.0.5 and 1.1.0 (07-Oct-2005 to 25-Jun-2009):

    *) Adjust copyright messages for years 2006-2009.
       [Ralf S. Engelschall]

    *) Support directories and files with spaces in the name.
       [Ralf S. Engelschall]

    *) Upgrade build environment to GNU autoconf 2.63 and GNU shtool 2.0.8
       [Ralf S. Engelschall]

  Changes between 1.0.4 and 1.0.5 (29-Sep-2005 to 07-Oct-2005):

    *) Fix diff(3) option fiddling introduced in version 1.0.4
       [Ralf S. Engelschall]

  Changes between 1.0.3 and 1.0.4 (18-Aug-2005 to 29-Sep-2005):

    *) Try diff(3) with options -U3, -u, -C3 and none (in this order)
       to be more platform neutral.
       [Ralf S. Engelschall]

    *) Upgraded to GNU shtool 2.0.3
       [Ralf S. Engelschall]

  Changes between 1.0.2 and 1.0.3 (06-Feb-2005 to 18-Aug-2005):

    *) Do not show a file as both modified and conflicting if both
       *.orig and *.rej exists. Instead list it just as conflicting
       as "cvs" does.
       [Ralf S. Engelschall]

  Changes between 1.0.1 and 1.0.2 (13-Jan-2005 to 06-Feb-2005):

    *) Replace unportable "if ! <cmd>" construct.
       [Ralf S. Engelschall]

  Changes between 1.0.0 and 1.0.1 (14-Dec-2004 to 13-Jan-2005):

    *) Adjust copyright messages for new year 2005.
       [Ralf S. Engelschall]

    *) Add a convenient "svs status" command which output a "cvs update"
       style list of files and their status (modification or conflict).
       [Ralf S. Engelschall]

    *) Remove *.rej files on "svs vi" if a modification was done.
       [Ralf S. Engelschall]

    *) Fix "make uninstall" procedure.
       [Ralf S. Engelschall]

    *) When editing a file look for corresponding .rej file
       and if found and the editor is Vim, open both files.
       [Ralf S. Engelschall]

    *) When searching for *.orig files, sort the resulting filename
       list to make sure we deterministically produce patches.
       [Ralf S. Engelschall]

  Changes between *GENESIS* and 1.0.0 (xx-Jun-2003 to 14-Dec-2004):

    *) Created the initial version of OSSP svs.
       [Ralf S. Engelschall]

